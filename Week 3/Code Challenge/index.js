const express = require("express");
const app = express();

const countryRoute = require("./routes/countryRoute");

//Use middleware to parse request body
app.use(express.json());
app.use(
	express.urlencoded({
		extended: true,
	})
);

app.use("/countries/", countryRoute);

app.listen(3000, () => console.log(`Server is running on port 3000`));
