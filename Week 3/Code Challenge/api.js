// ! THIS FILE IS USED TO FETCH COUNTRY LIST IN ASIA AND SAVE IT IN countries.json
const axios = require("axios");
const url = "https://restcountries.eu/rest/v2/region/asia";
const fs = require("fs");

async function writeJSONtoFile() {
	let response = await axios.get(url);
	let data = response.data;
	data = data.map((country) => {
		let countryObject = {};
		countryObject.id = parseInt(country.callingCodes[0]);
		countryObject.name = country.name;
		countryObject.capital = country.capital;
		return countryObject;
	});
	fs.writeFile("./models/countries.json", JSON.stringify(data), (err) => {
		if (err) throw err;
	});
}
writeJSONtoFile();
