This directory is my submission to Week 3 Code Challenge in Glints Academy #13.

This project creates a simple API server that supports CRUD operations. To start the server, you need to type these commands in your terminal. (Make sure port 3000 is available)

```sh
npm install
npm start
```

To test out the CRUD operations, I have included a postman collection saved as Code Challenge Week 3.postman_collection.json

File api.js is used to fetch countries in asia from external source and saved it in models/countries.json
