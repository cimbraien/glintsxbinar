let countries = require("../models/countries.json");

class CountryController {
	getAllCountries(req, res) {
		try {
			res.status(200).json({
				data: countries,
			});
		} catch (err) {
			res.status(500).json({
				message: err.message,
			});
		}
	}

	getCountryById(req, res) {
		try {
			let data = countries.filter((country) => req.params.id == country.id);
			if (data.length > 0) {
				res.status(200).json({
					data: data,
				});
			} else {
				res.status(418).json({
					message: `Country with id ${req.params.id} not found!`,
				});
			}
		} catch (err) {
			res.status(500).json({
				message: err.message,
			});
		}
	}

	getCountryByName(req, res) {
		try {
			let data = countries.filter((country) =>
				country.name.toLowerCase().includes(req.params.name.toLowerCase())
			);
			if (data.length > 0) {
				res.status(200).json({
					data: data,
				});
			} else {
				res.status(418).json({
					message: `Country with name like ${req.params.name} not found!`,
				});
			}
		} catch (err) {
			res.status(500).json({
				message: err.message,
			});
		}
	}

	addNewCountry(req, res) {
		try {
			let country = {
				id: req.body.id,
				name: req.body.name,
				capital: req.body.capital,
			};
			countries.push(country);
			res.status(201).json({
				data: country,
			});
		} catch (err) {
			res.status(500).json({
				message: err.message,
			});
		}
	}

	updateCountry(req, res) {
		try {
			let data = countries.filter((country) => req.params.id == country.id);
			let countryUpdated = {
				id: req.params.id,
				name: req.body.name,
				capital: req.body.capital,
			};
			if (data.length == 0) {
				countries.push(countryUpdated);
			} else {
				countries = countries.map((country) => {
					if (country.id == req.params.id) {
						return countryUpdated;
					} else {
						return country;
					}
				});
			}
			res.status(201).json({
				data: countryUpdated,
			});
		} catch (err) {
			res.status(500).json({
				message: err.message,
			});
		}
	}

	deleteCountry(req, res) {
		try {
			let matched = countries.filter((country) => country.id == req.params.id);
			countries = countries.filter((country) => country.id != req.params.id);
			if (matched.length > 0) {
				res.status(200).json({
					data: matched,
				});
			} else {
				res.status(204).send();
			}
		} catch (err) {
			res.status(500).json({
				message: err.message,
			});
		}
	}
}

module.exports = new CountryController();
