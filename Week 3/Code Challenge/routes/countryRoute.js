const router = require("express").Router();
const countryController = require("../controllers/countryController");

router.get("/", countryController.getAllCountries);

router.get("/id/:id", countryController.getCountryById);
router.get("/name/:name", countryController.getCountryByName);

router.post("/", countryController.addNewCountry);

router.put("/id/:id", countryController.updateCountry);

router.delete("/id/:id", countryController.deleteCountry);

module.exports = router;
