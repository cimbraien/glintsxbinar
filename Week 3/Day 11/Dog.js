const Animal = require("./Animal");

class Dog extends Animal {
	constructor(name, age, breed) {
		super(name, age);
		this.breed = breed;
	}

	printInfo() {
		super.printInfo();
		console.log(`This animal is a dog with breed ${this.breed}`);
	}
}

module.exports = Dog;
