const Animal = require("./Animal");

class Cat extends Animal {
	constructor(name, age, trait) {
		super(name, age);
		this.trait = trait;
	}

	printInfo() {
		super.printInfo();
		console.log(`This animal is a cat with distinct trait : ${this.trait}`);
	}
}

module.exports = Cat;
