class Animal {
	static population = 0;

	constructor(name, age) {
		this.name = name;
		this.age = age;
		Animal.population++;
	}

	printInfo() {
		console.log(`Animal with name ${this.name} has age of ${this.age}`);
	}
}

module.exports = Animal;
