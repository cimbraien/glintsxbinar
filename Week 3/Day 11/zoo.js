const Animal = require("./Animal");
const Dog = require("./Dog");
const Cat = require("./Cat");

const dog1 = new Dog("Heli", 5, "Dachshund");
const dog2 = new Dog("Gukguk", 2, "Poodle");
const dog3 = new Dog("Oscar", 3, "Pug");
const dog4 = new Dog("Kim", 4, "Shih Tzu");

const cat1 = new Cat("Meong", 1, "Friendly");
const cat2 = new Cat("Kitty", 5, "Fierce");
const cat3 = new Cat("P**sy", 3, "Dominant");

dog3.printInfo();
cat2.printInfo();
console.log("Animal population : ", Animal.population);
