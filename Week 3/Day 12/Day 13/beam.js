const ThreeDimension = require("../threeDimension");

class Beam extends ThreeDimension {
	constructor(length, width, height) {
		super("Beam");
		this.length = length;
		this.width = width;
		this.height = height;
	}

	// Call the parent method in this method
	whatWeAre() {
		this.Type();
	}

	// Overridding
	Type() {
		super.Type();
		console.log(`I am ${this.name}`);
	}

	// Overloading
	calculateArea(whoAreYou) {
		super.calculateArea();
		const yourName = `${whoAreYou} is trying to calculate the Beam Area: 2 x (pl + lt + pt) = `;
		console.log(
			`${yourName} ${
				2 *
				(this.length * this.width +
					this.width * this.height +
					this.length * this.height)
			} cm2`
		);
		return (
			2 *
			(this.length * this.width +
				this.width * this.height +
				this.length * this.height)
		);
	}

	calculateCircumference(whoAreYou) {
		super.calculateCircumference();
		const yourName = `${whoAreYou} is trying to calculate the Beam Circumference: 4 x (p + l + t) = `;
		console.log(
			`${yourName} ${4 * (this.length + this.width + this.height)} cm2`
		);
		return 4 * (this.length + this.width + this.height);
	}
}

module.exports = Beam;
