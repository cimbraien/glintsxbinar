const Geometry = require("./geometry");

class ThreeDimension extends Geometry {
  constructor(name) {
    super(name, "Three Dimension");

    if (this.constructot === ThreeDimension) {
      throw new Error("Three Dimension is an abstrak class");
    }
  }

  // Overridding
  Type() {
    super.Type();
    console.log(`I am ${this.type}`);
  }

  calculateArea() {
    console.log(`\n${this.name} Area!\n=====================`);
  }
  calculateCircumference() {
    console.log(`\n${this.name} Circumference!\n=====================`);
  }
  calculateVolume() {
    console.log(`\n${this.name} Volume!\n=====================`);
  }
}

module.exports = ThreeDimension;
