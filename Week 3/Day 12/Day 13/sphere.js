const ThreeDimension = require("./threeDimension");

class sphere extends ThreeDimension {
	constructor(radius) {
		super("sphere");
		this.radius = radius;
	}

	whoAmI() {
		super.Type();
		console.log(`I'm ${this.name}`);
	}

	calculateVolume(whoAreYou) {
		const who = `${whoAreYou} is trying to calculate Sphere Volume: `;
		let r3 = this.radius ** 3;
		return `${who} ${((4 / 3) * Math.PI * r3).toFixed(2)} cm3`;
	}

	calculateSurface(whoAreYou) {
		const who = `${whoAreYou} is trying to calculate Sphere Surface: `;
		let r2 = this.radius ** 2;
		return `${who} ${(4 * Math.PI * r2).toFixed(2)} cm2`;
	}
}

module.exports = sphere;
