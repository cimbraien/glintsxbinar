const axios = require("axios");

const url = "https://jsonplaceholder.typicode.com/posts/1";
const url1 = "https://jsonplaceholder.typicode.com/posts/1";
const url2 = "https://jsonplaceholder.typicode.com/posts/1/comments";
const url3 = "https://jsonplaceholder.typicode.com/comments?postId=1";

// Promise
// axios.get(url).then((response) => {
//   console.log(response.data);
// });

// Async await
async function fetch() {
	let response = await axios.get(url);
	console.log(response.data);

	response = await axios.get(url1);
	console.log(response.data);

	response = await axios.get(url2);
	console.log(response.data);

	response = await axios.get(url3);
	console.log(response.data);
}
fetch();
