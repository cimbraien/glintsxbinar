const fetch = require("node-fetch");

const url = "https://jsonplaceholder.typicode.com/posts/";
const url1 = "https://jsonplaceholder.typicode.com/posts/1";
const url2 = "https://jsonplaceholder.typicode.com/posts/1/comments";
const url3 = "https://jsonplaceholder.typicode.com/comments?postId=1";

// Promise
// fetch(url)
//   .then((res) => res.json())
//   .then((json) => console.log(json));

// Async await
async function fetchAPI() {
	try {
		let response = await fetch(url);
		let data = await response.json();
		console.log(data);

		response = await fetch(url1);
		data = await response.json();
		console.log(data);

		response = await fetch(url2);
		data = await response.json();
		console.log(data);

		response = await fetch(url3);
		data = await response.json();
		console.log(data);
	} catch (error) {
		console.error("Getting an error, exiting!");
	}
}
fetchAPI();
