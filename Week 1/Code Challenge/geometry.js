//Declarations of function to calculate a volume of geometry.
//Formulas source : https://www.varsitytutors.com/hotmath/hotmath_help/topics/perimeter-area-volume
let rectangle_prism = (length, width, height) => {
  return length * width * height;
};

let cube = (side) => {
  return rectangle_prism(side, side, side);
};

let cylinder = (radius, height) => {
  return Math.PI * radius ** 2 * height;
};

let cone = (radius, height) => {
  return cylinder(radius, height) / 3;
};

let sphere = (radius) => {
  return (cylinder(radius, radius) * 4) / 3;
};

//Call the functions and print on console
let geometries = [
  "prism with L=4, W=3, H=2",
  "cube with side length 4",
  "cylinder with r=3, H=4",
  "cone with r=3, H=4",
  "sphere with r=4",
];
let volumes = [
  rectangle_prism(4, 3, 2),
  cube(4),
  cylinder(3, 4),
  cone(3, 4),
  sphere(4),
];

for (let i = 0; i < geometries.length; i++) {
  console.log(`Volume of a ${geometries[i]} = ${volumes[i]}`);
}
