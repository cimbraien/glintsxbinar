//Declarations of function to calculate a volume of geometry.
//Formulas source : https://www.varsitytutors.com/hotmath/hotmath_help/topics/perimeter-area-volume
let rectangle_prism = (length, width, height) => {
  return length * width * height;
};

let cylinder = (radius, height) => {
  return Math.PI * radius ** 2 * height;
};

/* Unused formulas
let cube = (side) => {
  return rectangle_prism(side, side, side);
};

let cone = (radius, height) => {
  return cylinder(radius, height) / 3;
};

let sphere = (radius) => {
  return (cylinder(radius, radius) * 4) / 3;
}; */

//Import readLine library
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

//Check if input is a number between 1 or 2
let validateGeometry = (geometry) => {
  if (isNaN(geometry)) {
    console.log("Choice must be a number!");
  } else if (geometry < 1 || geometry > 5) {
    console.log("Choice can only be 1 or 2");
  } else {
    return true;
  }
  return false;
};

//Check if input is a number
let validateInput = (name, input) => {
  if (isNaN(input)) {
    console.log(`${name} must be a number!`);
  } else if (input <= 0) {
    console.log(`${name} must be a positive number!`);
  } else {
    return true;
  }
  return false;
};

//Input function
let input = () => {
  const geoFunc = [rectangle_prism, cylinder];
  const args = [
    ["length", "width", "height"],
    ["radius", "height"],
  ];

  console.log("Choose which volume geometry to calculate.");
  geoFunc.forEach((func, i) => {
    console.log(i + 1, func.name);
  });
  rl.question("Geometry = ", (geometry) => {
    if (!validateGeometry(geometry)) rl.close();
    func = geoFunc[geometry - 1];
    arg = args[geometry - 1];

    rl.question(`${arg[0]}: `, (arg0) => {
      if (!validateInput(arg[0], arg0)) rl.close();
      rl.question(`${arg[1]}: `, (arg1) => {
        if (!validateInput(arg[1], arg1)) rl.close();
        //If chosen geometry is prism, take another input as argument
        if (geometry == 1) {
          rl.question(`${arg[2]}: `, (arg2) => {
            if (!validateInput(arg[2], arg2)) rl.close();
            let volume = func(arg0, arg1, arg2);
            console.log(
              `Volume of ${func.name} with L=${arg0}, W=${arg1}, H=${arg2} = ${volume}`
            );
            rl.close();
          });
          //If chosen geomtry is cylinder, immediately calculate and print result
        } else {
          let volume = func(arg0, arg1);
          console.log(
            `Volume of ${func.name} with r=${arg0}, H=${arg1} = ${volume}`
          );
          rl.close();
        }
      });
    });
  });
};
input();

/* Unused method of printing result
//Call the functions and print on console
let geometries = [
  "prism with L=4, W=3, H=2",
  "cube with side length 4",
  "cylinder with r=3, H=4",
  "cone with r=3, H=4",
  "sphere with r=4",
];
let volumes = [
  rectangle_prism(4, 3, 2),
  cube(4),
  cylinder(3, 4),
  cone(3, 4),
  sphere(4),
];

for (let i = 0; i < geometries.length; i++) {
  console.log(`Volume of a ${geometries[i]} = ${volumes[i]}`);
}
 */
