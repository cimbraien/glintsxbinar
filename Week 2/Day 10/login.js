const EventEmitter = require("events");
const readline = require("readline");
const ass2 = require("../Day 9/assignment2");

const e = new EventEmitter();
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const accounts = [
  {
    username: "cimbraien",
    PIN: 133769,
  },
  {
    username: "admin",
    PIN: 654321,
  },
  {
    username: "lordreza",
    PIN: 420420,
  },
];

// Login Listeners
e.on("loginFailed", (username, errId) => {
  // ? errId 0 = username not found; 1 = wrong pin; 2 = wrong format
  switch (errId) {
    case 0:
      console.log(`Username ${username} is not found!`);
      break;
    case 1:
      console.log(`Wrong PIN for username ${username}!`);
      break;
    case 2:
      console.log(`Wrong username format -> (${username})`);
      break;
  }
  rl.close();
});

e.on("loginSuccess", (username) => {
  console.log(`User ${username} logged in successfully.`);
  console.log(`Calling assignment2.js ...`);
  setTimeout(() => {
    rl.close();
    ass2.start();
  }, 1000);
});

//Retrieve account from accounts array. Return false if no username found.
let getAccount = (username) => {
  let out = false;
  accounts.forEach((account) => {
    if (account.username == username) {
      out = account;
    }
  });
  return out;
};

let login = () => {
  console.log(`Welcome to the login screen! Pleas input your username and PIN`);
  rl.question("Username: ", (username) => {
    rl.question("PIN: ", (pin) => {
      if (username.match(/ /) != null) {
        e.emit("loginFailed", username, 2);
        return;
      } else {
        account = getAccount(username);
        console.log(account);
        if (!account) {
          e.emit("loginFailed", username, 0);
        } else if (account.PIN != pin) {
          e.emit("loginFailed", username, 1);
        } else {
          e.emit("loginSuccess", username);
        }
      }
    });
  });
};

login();
