This code challenge is to create bubble sort algorithm.

index.js contains the algorithm to sort array in ascending and descending order with bubble sort.

lib directory contains the scripts to produce random number array and test whether the algorithm works.
