const data = require("./lib/arrayFactory.js");
const test = require("./lib/test.js");

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter((i) => typeof i === "number");
}

// Should return array
function sortAscending(data) {
  // Code Here
  let d = clean(data);
  let isSorted = false;
  while (!isSorted) {
    isSorted = true;
    for (let i = 0; i < d.length - 1; i++) {
      //Check if current element is greater than next element
      if (d[i] > d[i + 1]) {
        isSorted = false;
        //Swap current element with the next
        let temp = d[i];
        d[i] = d[i + 1];
        d[i + 1] = temp;
      }
    }
  }
  return d;
}

// Should return array
function sortDecending(data) {
  // Code Here
  let d = clean(data);
  let isSorted = false;
  while (!isSorted) {
    isSorted = true;
    //Check if current element is lower than next element
    for (let i = 0; i < d.length - 1; i++) {
      if (d[i] < d[i + 1]) {
        isSorted = false;
        //Swap current element with the next
        let temp = d[i];
        d[i] = d[i + 1];
        d[i + 1] = temp;
      }
    }
  }
  return d;
}

// DON'T CHANGE
test(sortAscending, sortDecending, data);
