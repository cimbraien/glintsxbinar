//Import readLine library
let rl = null;
let importReadline = () => {
  const readline = require("readline");
  rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
};

const people = [
  {
    name: "John",
    status: "Positive",
  },
  {
    name: "Mike",
    status: "Suspect",
  },
  {
    name: "Jason",
    status: "Positive",
  },
  {
    name: "Britney",
    status: "Suspect",
  },
  {
    name: "Ella",
    status: "Negative",
  },
];

function start() {
  if (rl == null) importReadline();
  console.log("");
  console.log("======================================");
  console.log("Status : [Positive, Suspect, Negative]");
  rl.question("Choose which status to list : ", (status) => {
    let names = [];
    switch (status.toLowerCase()) {
      case "positive":
        people.forEach((person) => {
          if (person.status == "Positive") names.push(person.name);
        });
        break;
      case "suspect":
        people.forEach((person) => {
          if (person.status == "Suspect") names.push(person.name);
        });
        break;
      case "negative":
        people.forEach((person) => {
          if (person.status == "Negative") names.push(person.name);
        });
        break;
      default:
        console.log("Status not found!");
        start();
        return;
    }
    //Print name list
    console.log(names.toString() + " is " + status.toLowerCase());
    start();
  });
}

module.exports = { start };
