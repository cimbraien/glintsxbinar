let vegetables = ["tomato", "broccoli", "kale", "cabbage", "apple"];

vegetables.forEach((veg) => {
  if (veg != "apple") {
    console.log(`${veg} is a healthy food, it's definitely worth to eat.`);
  }
});
