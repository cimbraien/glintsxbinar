const isTicket = false;
const isGeNoseNegative = false;
const isAntigenNegative = false;

function checkTicketOnline() {
  console.log("Run traveloka");
  isTicketAvailable();
}
function isTicketAvailable() {
  console.log("Checking ticket ...");
  if (isTicket === false) {
    console.log("ticket not found");
    goToStation();
    return;
  }
  console.log("ticket found");
  buyTicketOnline();
}
function buyTicketOnline() {
  console.log("ticket bought");
  chooseSeat();
}
function chooseSeat() {
  console.log("choosing a seat...");
  console.log("seat chosen");
  initGeNose();
}
function initGeNose() {
  console.log("initiating GeNose test...");
  genoseResult(isGeNoseNegative);
}
function genoseResult(isGeNoseNegative) {
  if (isGeNoseNegative === false) {
    console.log("You're positive, please check antigen");
    initAntigen(isAntigenNegative);
    return;
  }
  console.log("You're negative");
  checkIn();
}
function checkIn() {
  console.log("Validating ticket ...");
  console.log("ticket validated");
  boardTrain();
}
function boardTrain() {
  console.log("boarding train");
  trainDeparted();
}
function trainDeparted() {
  console.log("train has departed");
}
function goToStation() {
  console.log("go to station");
  buyTicketCounter();
}
function buyTicketCounter() {
  console.log("ticket bought");
  chooseSeat();
}
function initAntigen() {
  console.log("initiating Antigen test...");
  antigenResult(isAntigenNegative);
}
function antigenResult(isAntigenNegative) {
  if (isAntigenNegative === false) {
    console.log("You're positive");
    console.log("Sorry, you can't get on the train");
    medicalCheckUp();
    return;
  }
  console.log("You're negative");
  checkIn();
}
function medicalCheckUp() {
  console.log("You should check your condition");
}
function start() {
  checkTicketOnline();
}
start();
